﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR;
public class ControllerPresence : MonoBehaviour
{
    // Start is called before the first frame update
    public InputDeviceCharacteristics ControlloerCharac;
    public GameObject ControllerModel;

    private GameObject controller;
    private InputDevice input;
    void Start()
    {
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(ControlloerCharac,devices);

        if(devices.Count>0)
        {
            input = devices[0];
            controller= Instantiate(ControllerModel,transform);
            controller.transform.Rotate(new Vector3(0,180,0));
        }
    }

    // Update is called once per frame
    void Update()
    {
        input.TryGetFeatureValue(CommonUsages.grip,out float GripValue);
        input.TryGetFeatureValue(CommonUsages.primaryButton,out bool primaryButtonValue);
        input.TryGetFeatureValue(CommonUsages.primary2DAxis,out Vector2 primary2DAxisValue);

        // if(GripValue>=0.1)
        //     Debug.Log("Grip pressed -> "+GripValue);

        // if(primaryButtonValue)
        //     Debug.Log("primaryButton pressed  ");

        // if(primary2DAxisValue != Vector2.zero)
        //     Debug.Log("primary2DAxisValue pressed  -> "+primary2DAxisValue); 
    }
}

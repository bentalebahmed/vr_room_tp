﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    public InputDeviceCharacteristics ControlloerCharac;
    private InputDevice input;
    private XRRig camera;
    private CharacterController character;
    public float speed = 2.0f;
    void Start()
    {
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(ControlloerCharac,devices);

        if(devices.Count>0)
        {
            input = devices[0];
        }

        character = GetComponent<CharacterController>();
        camera = GetComponent<XRRig>();
        
    }

    // Update is called once per frame
    void Update()
    {
        input.TryGetFeatureValue(CommonUsages.primary2DAxis,out Vector2 primary2DAxisValue);

        Vector3 direction = new Vector3(primary2DAxisValue.x,0,primary2DAxisValue.y);
        Quaternion headDirection = Quaternion.Euler(0,camera.cameraGameObject.transform.eulerAngles.y,0);
        character.Move(headDirection * direction * Time.deltaTime *speed);
    }
}

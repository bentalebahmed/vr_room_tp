﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.UI;
public class ImageScroll : MonoBehaviour
{
    public InputDeviceCharacteristics ControllerChar;
    private InputDevice input;
    private RawImage sagittal;
    private RawImage coronal;
    private RawImage axial;
    private Texture2D myTexture;
    private int imageIndex = 1;
    private bool scanned = false; 
    // Start is called before the first frame update
    void Start()
    {
        List<InputDevice> devices = new List<InputDevice>();

        InputDevices.GetDevicesWithCharacteristics(ControllerChar,devices);

        if(devices.Count>0)
        {
            input = devices[0];
        }


        axial= GameObject.Find("Axial").GetComponent<RawImage>();
        sagittal= GameObject.Find("Sagittal").GetComponent<RawImage>();
        coronal = GameObject.Find("Coronal").GetComponent<RawImage>();
    }

    // Update is called once per frame
    void Update()
    {
        
        input.TryGetFeatureValue(CommonUsages.primaryButton,out bool pBtnValue);
        if(scanned)
        {
            if(pBtnValue)
            {   
                string  AxialPath = "Axial/dcm_CTHead"+imageIndex; 
                string  saggitalPath = "Sagittal/dcm_CTHead"+imageIndex; 
                string  CoronalPath = "Coronal/dcm_CTHead"+imageIndex; 

                myTexture = Resources.Load(AxialPath) as Texture2D;
                axial.GetComponent<RawImage>().texture = myTexture;

                myTexture = Resources.Load(saggitalPath) as Texture2D;
                sagittal.GetComponent<RawImage>().texture = myTexture;

                myTexture = Resources.Load(CoronalPath) as Texture2D;
                coronal.GetComponent<RawImage>().texture = myTexture;

                imageIndex++;
            }
        }

        //Challenge
        //Secondary button -> décrimenter les images
        
    }

    public void setScanned(bool b)
    {
        scanned = b;
    }
}
